import * as types from '../actions/actionTypes';
import {combineReducers} from 'redux';


function memes(state=[], action){
    switch(action.type){
        case types.RECEIVE_MEMES:
        return action.memes;
        default:
            return state;
    }
}

function myMemes(state=[], action){
    switch(action.type){
        case 'NEW_MEME':
            return [...state, action.meme];
        default:
            return state;
            
    }
}

const rootReducer = combineReducers({
    memes,
    myMemes
})

export default rootReducer;