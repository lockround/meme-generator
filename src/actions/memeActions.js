import * as types from './actionTypes';
import {username,password} from './secrets';

function recieveMemes(json){
    const {memes} = json.data;
    return {
        type: types.RECEIVE_MEMES,
        memes
    }
}

function fetchMemesJson(){
    return fetch('https://api.imgflip.com/get_memes')
    .then(resp => resp.json())
}

export default function fetchMemes(){
    return function(dispatch){
        return fetchMemesJson().then(resp => dispatch(recieveMemes(resp))).catch(error => {
            throw(error);
        })
    }
}


function newMeme(meme){
    return {
        type: types.NEW_MEME,
        meme
    }
}

function postMemeJson(params){
    params["username"] = username;
    params["password"] = password;
    const bodyParams = Object.keys(params).map(key => {
        return encodeURIComponent(key)+'='+encodeURIComponent(params[key])
    }).join('&');
    
    return fetch('https://api.imgflip.com/caption_image', {
        method: 'POST',
        headers: {
            'Content-Type':'application/x-www-form-urlencoded'
        },
        body: bodyParams
    }).then(response => response.json());
}


export function createMeme(new_meme_object){
    return function (dispatch){
        return postMemeJson(new_meme_object)
        .then(new_meme => dispatch(newMeme(new_meme)))
    }
}






















