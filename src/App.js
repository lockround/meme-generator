import React from 'react';
import {connect} from 'react-redux';
import MemeItem from './components/MemeItem';
import MyMemes from './components/MyMemes';
import  {Form, FormGroup, FormControl, ControlLabel} from 'react-bootstrap';
import './styles/index.css';

function mapStateToProps(state){
    return state;
}

class App extends React.Component {
    constructor(){
        super();
        this.state = {
            memeLimit: 10,
            text0: '',
            text1: ''
        }
    }
    render(){
        return (<div>
            <h1><u>Welcome to MEME generator!</u></h1>
            <MyMemes/>
            <h4><i>Write some text</i></h4>
            <Form inline>
                <FormGroup>
                    <ControlLabel>Top</ControlLabel>
                    {' '}
                    <FormControl 
                        type="text"
                        onChange={(event) => this.setState({text0: event.target.value})}
                        ></FormControl>
                </FormGroup>
                {' '}
                <FormGroup>
                    <ControlLabel>
                        Bottom
                    </ControlLabel>
                    {' '}
                    <FormControl 
                        type="text"
                        onChange={(event) => this.setState({text1: event.target.value})}
                        ></FormControl>
                </FormGroup>
            </Form>
            {this.props.memes.slice(0,this.state.memeLimit).map((meme,i)=>{
                return(
                <MemeItem
                    key={meme.id}
                    meme={meme}
                    text0={this.state.text0}
                    text1={this.state.text1}
                />
                ) 
            })}
            
            <div className="button" 
            onClick={() => this.setState({memeLimit: this.state.memeLimit+10})}>
                Load 10 more...
            </div>
            <div className="bottom">
                by \`-`/ shubham tiwari
            </div>
            </div>
        )    
    }
    
}

export default connect(mapStateToProps,null)(App);
















